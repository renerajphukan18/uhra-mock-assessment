# UHRA Algorithmic Challenges

Welcome to the UHRA Algorithmic Challenges Mock-Asssessment! Here, you'll find a set of algorithmic challenges designed to assess your skills in programming and problem solving related to autonomous vehicles and Formula Student competitions.

## How to Participate

To participate in these challenges, follow these steps:

1. **Fork this repository** to your own GitLab account.

2. **Clone your forked repository** to your local machine:

```git clone https://gitlab.com/your-username/uhra-assessment/algorithmic-problem-solving.git```

3. Inside the repository, you'll find a set of coding challenges categorized by difficulty level: Beginner, Intermediate, and Advanced.

4. **Choose a challenge** that matches your skill level and interest.

5. **Solve the challenge** by writing Python code a Python file (e.g., `Solutions/challenge-1-John-Simth.py`, `Solutions/challenge-2-Emma-Snow.py`, etc.). Include comments to explain your approach.

6. **Commit your completed challenge** to your local repository in desingated difficulty level folder inside solutions directory:

```git add challenge-X-your-name.py # Replace X with the challenge number```

```git commit -m "Completed Challenge X - Your Name" # Replace X with the challenge number```

7. **Push your changes** to your GitHub fork:

```git push origin main```

8. **Create a Pull Request (PR)** from your forked repository's main branch to the `main` branch of this original repository.

9. In the PR description, provide a brief summary of your solution and any insights gained during the challenge.

10. **Your Answers will NOT be reviewed**, and The answer to these questions shall be pushed to Main Repository A DAY before the ASSESSMENT.

## Challenge Descriptions

### Beginner Level Challenges

1. **Challenge 1: Sorting Algorithm**: Implement the quicksort algorithm to sort a list of integers. Provide the sorted list and the steps you took to sort it.

2. **Challenge 2: Graph Algorithm**: Explain the process of performing a breadth-first search (BFS) on a directed graph, starting from a specific node. Describe the order in which nodes are visited and provide a high-level algorithm or pseudocode for this traversal.

### Intermediate Level Challenges

3. **Challenge 3: Dynamic Programming**: Find the length of the longest increasing subsequence, where the subsequence is not necessarily continuous. In other words, you need to find the length of the longest subsequence of elements in a given set of positive integers that is in strictly increasing order, but the elements do not have to be adjacent to each other in the original set.

4. **Challenge 4: Search Algorithm**: You are given a rotated sorted array of integers (an array that was sorted and then rotated), and a target value. Write a Python program to search for the target value in the rotated sorted array using binary search.

### Advanced Level Challenge

5. **Challenge 5: Complex Graph Algorithm**: You are given a weighted, directed graph with nodes and edges, but this time the graph may contain negative edge weights. Write pseudocode or provide a step-by-step description of how you would find the shortest path between two given nodes in the graph using Dijkstra's algorithm, considering the possibility of negative edge weights and handling any resulting complexities.

## Have Fun and Learn!

These challenges offer an opportunity to test your problem-solving skills in the context of Formula Student AI. Enjoy the challenges, learn from them, and showcase your abilities. Good luck!
