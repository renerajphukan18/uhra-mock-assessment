# UHRA Logical Reasoning Challenges

Welcome to the UHRA Logical Reasoning Challenges Mock-Asssessment! Here, you'll find a set of legocal reasoning challenges designed to assess your critical thinking and analysing skills related to autonomous vehicles and Formula Student competitions.

## How to Participate

To participate in these challenges, follow these steps:

1. **Fork this repository** to your own GitHub account.

2. **Clone your forked repository** to your local machine:

```git clone https://gitlab.com/your-username/uhra-assessment/logical-reasoning.git```

3. Inside the repository, you'll find a set of coding challenges categorized by difficulty level: Beginner, Intermediate, and Advanced.

4. **Choose a challenge** that matches your skill level and interest.

5. **Solve the challenge** by writing Python code a Python file (e.g., `Solutions/challenge-1-John-Simth.py`, `Solutions/challenge-2-Emma-Snow.py`, etc.). Include comments to explain your approach.

6. **Commit your completed challenge** to your local repository in desingated difficulty level folder inside solutions directory:

```git add challenge-X-your-name.py # Replace X with the challenge number```

```git commit -m "Completed Challenge X - Your Name" # Replace X with the challenge number```

7. **Push your changes** to your GitHub fork:

```git push origin main```

8. **Create a Pull Request (PR)** from your forked repository's main branch to the `main` branch of this original repository.

9. In the PR description, provide a brief summary of your solution and any insights gained during the challenge.

10. **Your Answers will NOT be reviewed**, and The answer to these questions shall be pushed to Main Repository A DAY before the ASSESSMENT.

## Challenge Descriptions

### Beginner-Level Challenges

1. **Challenge 1 Solve the following logical reasoning puzzle:**

Three friends decide to stay at a hotel room, which costs a total of $30. They each contribute $10, handing $30 to the hotel clerk. Later, the hotel clerk realizes there's a special promotion and the room should only cost $25. The hotel clerk gives $5 to the bellboy and asks him to return it to the friends.

The bellboy, however, decides to keep $2 for himself and gives $1 back to each of the three friends. Now, each of the friends has paid $9 (a total of $27) for the room, and the bellboy has kept $2, which adds up to $29.

What happened to the missing dollar?

Write your answer in the logical_reasoning.txt file.

2. **Challenge 2 Solve the following logical reasoning puzzle:**

In a small village, there was a peculiar tradition. Each year, on a specific day, a person was chosen to be the "Village Truth Teller." The chosen person was always someone who never told lies. This year, it was John who was selected as the Village Truth Teller.

On the day of the selection, John made the following statement: "I will never be the Village Truth Teller again."

The villagers were puzzled. If John was telling the truth, it meant he would never be the Truth Teller again, which contradicted his statement. If he was lying, it meant he would be the Truth Teller again, which also contradicted his statement.

The villagers were left scratching their heads. Can you figure out whether John was telling the truth or lying, and why?

This riddle challenges you to think about the paradoxical nature of John's statement and whether it's possible for him to be telling the truth or lying.
Write your answer in the logical_reasoning.txt file.



### Intermediate-Level Challenges

3. **Challenge 3 Solve the following logical reasoning puzzle:**

A group of five friends decided to go hiking in the forest one sunny afternoon. They each took a different type of fruit with them, wore a different color of hat, and carried a different type of backpack. Can you determine who brought the oranges?

Clues:

Alex is not wearing the red hat and does not have the blue backpack.

The person with the green hat is carrying a black backpack.

Lisa has the orange backpack.

The one with the red hat is not carrying the green backpack.

The backpack with stripes belongs to the person with the yellow hat.

The one with the white backpack didn't bring apples.

Peter is not wearing the green hat and doesn't have the yellow backpack.

The person who has bananas is wearing a blue hat.


Write your answer in the logical_reasoning.txt file.

4. **Challenge 4 Solve the following logical reasoning puzzle:**

You are on an island inhabited by three tribes: the Red Hats, the Blue Hats, and the Green Hats. Each tribe has a chief, and each chief wears a hat of their tribe's color. The Red Hats always tell the truth, the Blue Hats always lie, and the Green Hats sometimes tell the truth and sometimes lie.

One day, you come across three individuals from different tribes, and they are wearing hats. You don't know which tribe each person belongs to, and they won't tell you. They are standing in a line, and you can ask upto three yes-or-no questions to determine the tribe of each person.

What questions can you ask to figure out the tribe of each person?

Write your answer in the logical_reasoning.txt file.

### Advanced Level Challenge

5. **Challenge 5 Solve the following logical reasoning puzzle:**

The Stolen Diamond Necklace

In a high-security museum, a priceless diamond necklace was stolen from a glass case. The case was locked, and the alarms were working perfectly. There were only two guards on duty that night.

Guard A said, "I was patrolling the corridors when the theft occurred. I didn't see anyone near the exhibit, and I have a witness who can vouch for me."

Guard B stated, "I was in the control room, monitoring the security cameras. I can prove that I didn't leave the room, and there's no footage of anyone approaching the glass case."

The police reviewed the security footage and confirmed that Guard B never left the control room, and Guard A's witness confirmed his alibi.

Despite this, the police were able to determine who the thief was. How did they do it, and who was the thief?

Write your answer in the logical_reasoning.txt file.

## Have Fun and Learn!

These challenges offer an opportunity to test your critical thinking and analysing skills in the context of Formula Student AI. Enjoy the challenges, learn from them, and showcase your abilities. Good luck! And remember You're Kenough!
