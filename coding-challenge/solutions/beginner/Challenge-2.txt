You can create a Python function `find_missing_numbers` to find elements that are in `list1` but not in `list2` without duplicates by using sets. Here's a sample implementation:

```python
def find_missing_numbers(list1, list2):
    set1 = set(list1)
    set2 = set(list2)

    missing_numbers = list(set1 - set2)

    return missing_numbers

# Example usage:
list1 = [1, 2, 3, 4, 5]
list2 = [3, 4, 5, 6, 7]
result = find_missing_numbers(list1, list2)
print("Missing numbers in list1:", result)
```

In this code, we first convert both `list1` and `list2` into sets using `set()`. Then, we use the set difference operation (`-`) to find elements that are in `set1` but not in `set2`, which gives us the missing numbers. Finally, we convert the result set back into a list and return it.

The example usage will print the missing numbers in `list1` that are not present in `list2`. In this case, it will be `[1, 2]`.